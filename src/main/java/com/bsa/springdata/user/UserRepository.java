package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT user FROM User user LEFT JOIN user.office office WHERE office.city = :city")
    List<User> findByCity(@Param("city") String city, Sort sort);

    List<User> findByExperienceGreaterThanEqual(int experience, Sort sort);

    @Query("SELECT user FROM User user LEFT JOIN user.office office LEFT JOIN user.team team  " +
            "WHERE office.city = :city AND team.room = :room")
    List<User> findByRoomAndCity(@Param("room") String room, @Param("city") String city, Sort sort);

   int deleteByExperienceLessThan(int experience);

}
