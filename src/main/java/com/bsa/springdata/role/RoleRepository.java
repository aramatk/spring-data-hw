package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    @Modifying
    @Query("DELETE FROM Role role WHERE role.code = :code AND role.users.size = 0")
    void deleteByCode(String code);
}
