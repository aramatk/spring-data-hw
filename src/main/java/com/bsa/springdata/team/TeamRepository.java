package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);

    int countByTechnologyName(String name);

    @Query("SELECT DISTINCT team FROM Team team LEFT JOIN team.users user " +
            "WHERE team.technology.name = :technologyName GROUP BY  team HAVING COUNT(user) < :usersCount")
    List<Team> findByTechnologyNameAndUsersCountLessThan(@Param("technologyName") String technologyName,
                                                         @Param("usersCount") long usersCount);

    @Modifying
    @Query(value = "UPDATE teams team SET name = CONCAT(team.name,'_',p.name,'_',tech.name) " +
            "FROM projects p, technologies tech " +
            "WHERE p.id = team.project_id AND tech.id = team.technology_id AND team.name = :teamName",
            nativeQuery = true)
    void normalizeName(@Param("teamName") String teamName);
}
