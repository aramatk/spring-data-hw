package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        List<Team> teams = teamRepository.findByTechnologyNameAndUsersCountLessThan(oldTechnologyName, devsNumber);
        teams.forEach(team -> {
            team.getTechnology().setName(newTechnologyName);
            technologyRepository.save(team.getTechnology());
        });
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
