package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.Technology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findByTechnology(technology, PageRequest.of(0, 5)).stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return projectRepository.findTheBiggest(PageRequest.of(0, 1)).stream()
                .map(ProjectDto::fromEntity)
                .findFirst();
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummaryList();
    }

    public int getCountWithRole(String role) {
        return projectRepository.countByRoleName(role);
    }

    @Transactional
    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequestDto) {
        Technology technology = Technology.builder()
                .name(createProjectRequestDto.getTech())
                .description(createProjectRequestDto.getTechDescription())
                .link(createProjectRequestDto.getTechLink())
                .build();
        Team team = Team.builder()
                .name(createProjectRequestDto.getTeamName())
                .area(createProjectRequestDto.getTeamArea())
                .room(createProjectRequestDto.getTeamRoom())
                .technology(technology)
                .build();
        Project project = Project.builder()
                .name(createProjectRequestDto.getProjectName())
                .description(createProjectRequestDto.getProjectDescription())
                .teams(Arrays.asList(team))
                .build();
        Project savedProject = projectRepository.save(project);
        return savedProject.getId();
    }
}
