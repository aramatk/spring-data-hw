package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("SELECT project FROM Project project " +
            "LEFT JOIN project.teams team " +
            "LEFT JOIN team.technology t " +
            "WHERE t.name = :technology " +
            "ORDER BY team.users.size desc")
    List<Project> findByTechnology(@Param("technology") String technology, Pageable pageable);

    Optional<Project> findByName(String name);

    @Query("SELECT project FROM Project project " +
            "LEFT JOIN project.teams team " +
            "ORDER BY project.teams.size desc , team.users.size desc, project.name desc ")
    List<Project> findTheBiggest(Pageable pageable);

    @Query("SELECT COUNT(DISTINCT project.id) FROM Project project " +
            "LEFT JOIN project.teams team " +
            "LEFT JOIN team.users user " +
            "LEFT JOIN user.roles role " +
            "WHERE role.name = :roleName")
    int countByRoleName(@Param("roleName") String roleName);

    @Query(value = "SELECT DISTINCT " +
            "p.name, COUNT(DISTINCT t.id) teamsNumber, COUNT(DISTINCT u.id) developersNumber, " +
            "STRING_AGG(DISTINCT tech.name, ',' ORDER BY tech.name DESC) Technologies " +
            "FROM projects p, teams t, users u, technologies tech " +
            "WHERE p.id = t.project_id AND t.id = u.team_id AND tech.id = t.technology_id " +
            "GROUP BY p.name " +
            "ORDER BY p.name",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummaryList();
}

