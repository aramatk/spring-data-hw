package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("SELECT DISTINCT office FROM Office office " +
            "LEFT JOIN office.users user " +
            "WHERE user.team.technology.name = :technology")
    List<Office> findByTechnology(@Param("technology") String technology);

    @Modifying
    @Query(value = "UPDATE offices o SET address = :newAddress " +
            "FROM users u, teams t " +
            "WHERE o.id = u.office_id AND t.id  = u.team_id AND t.project_id IS NOT NULL AND o.address = :oldAddress",
            nativeQuery = true)
    void updateAddress(@Param("oldAddress") String oldAddress, @Param("newAddress") String newAddress);

    List<Office> findByAddress(String address);
}
